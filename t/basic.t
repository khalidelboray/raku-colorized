use Test;
use Colorizable;

subtest 'test color and text effect application', {
    my $string = 'This is a string';
    my $color-string = $string but Colorizable;

    is $color-string.colorize(:fg(blue)),
    "\x[1B][0;34;49m$string\x[1B][0m",
    'named enumeration for foreground as blue'
    ;
    
    is $color-string.colorize(:fg('blue')),
    "\x[1B][0;34;49m$string\x[1B][0m",
    'named string for foreground as blue'
    ;
    
    is $color-string.colorize(blue),
    "\x[1B][0;34;49m$string\x[1B][0m",
    'positional enumeration for foreground as blue'
    ;
    
    is $color-string.colorize('blue'),
    "\x[1B][0;34;49m$string\x[1B][0m",
    'positional string for foreground as blue'
    ;
    
    is $color-string.colorize(light-blue),
    "\x[1B][0;94;49m$string\x[1B][0m",
    'positional enumeration for foreground as light blue'
    ;
    
    is $color-string.colorize(fg => light-blue, bg => red),
    "\x[1B][0;94;41m$string\x[1B][0m",
    'named enumerations for light blue foreground and red background'
    ;
    
    is $color-string.colorize(fg => light-blue, bg => red, mo => bold),
    "\x[1B][1;94;41m$string\x[1B][0m",
    'named enumerations for light blue foreground, red background, and bold text'
    ;
}

subtest 'test color/mode methods', {
    my $string = 'This is a string';
    my $color-string = $string but Colorizable;

    is $color-string.blue,
    "\x[1B][0;34;49m$string\x[1B][0m",
    'blue text via method';

    is $color-string.yellow,
    "\x[1B][0;33;49m$string\x[1B][0m",
    'yellow text via method';

    is $color-string.yellow.on-red,
    "\x[1B][0;33;41m$string\x[1B][0m",
    'yellow text on red via methods';

    is $color-string.colorize(magenta).on-cyan,
    "\x[1B][0;35;46m$string\x[1B][0m",
    'magenta text on cyan via positional and method';

    is $color-string.colorize(blue).on-red,
    "\x[1B][0;34;41m$string\x[1B][0m",
    'blue text on red via positional and method';

    is $color-string.colorize(red).on-blue.underline,
    "\x[1B][4;31;44m$string\x[1B][0m",
    'underlined red text on blue via positional and methods';
}

subtest 'test uncolorize/is-colorized methods', {
    my $string = 'This is a string';
    my $color-string = $string but Colorizable;

    is $color-string.colorize(:blue).uncolorize,
    $string,
    'string is uncolorized after call to uncolorize';

    is True,  ('Red' but Colorizable).colorize(red).is-colorized, 'string is colorized';
    is False, ('Blue' but Colorizable).is-colorized, 'string is not colorized';
    is False, ('Green' but Colorizable).colorize(green).uncolorize.is-colorized, 'string is not colorized after call to uncolorize';
}

subtest 'test for invalid colors/modes', {
    throws-like { ("Blue" but Colorizable).colorize(:fg('ablue')) },
    Exception, 'dies on invalid string color', message => /'valid color'/;

    throws-like { ("Blue" but Colorizable).colorize(:mo('overline')) },
    Exception, 'dies on invalid string mode', message => /'valid mode'/;

    throws-like { ("Blue" but Colorizable).on-blua },
    Exception, 'dies on invalid method for background'; #message => /'valid mode'/;

    throws-like { ("Blue" but Colorizable).alternating },
    Exception, 'dies on invalid method for mode'; #message => /'valid mode'/;
}

subtest 'test newline', {
    my $string = "This is\na string";
    my $color-string = $string but Colorizable;

    is $color-string.colorize(blue, red, blink),
    "\x[1B][5;34;41m$string\x[1B][0m",
    'string with newline'
    ;
}

subtest 'composing Colorizable into a regular class', {
    class A does Colorizable {
        method Str { "I'm class A" }
    }

    is A.colorize(:fg(blue)),
    "\x[1B][0;34;49mI'm class A\x[1B][0m",
    "use of the class's Str method"
    ;
}

subtest 'augmenting a class with Colorizable', {
    use MONKEY-TYPING;

    augment class Str {
        also does Colorizable;
    }
    
    my $string = 'This is a string';

    is $string.colorize(:fg(blue)),
    "\x[1B][0;34;49m$string\x[1B][0m",
    'named enumeration for foreground as blue'
    ;
    
    is $string.colorize(fg => light-blue, bg => red),
    "\x[1B][0;94;41m$string\x[1B][0m",
    'named enumerations for light blue foreground and red background'
    ;

    is $string.colorize(fg => light-blue).colorize(bg => red),
    "\x[1B][0;94;41m$string\x[1B][0m",
    'named enumerations for light blue foreground and red background via method chaining'
    ;
    
    is $string.colorize(:fg(red)).colorize(bg => yellow),
    "\x[1B][0;31;43m$string\x[1B][0m",
    'named enumerations for red foreground and yellow background via method chaining'
    ;
 
}

done-testing;
